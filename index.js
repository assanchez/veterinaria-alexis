const app = require("./app.js")

app.listen(app.get("port"),() =>{ 
    console.log('Running on Port', app.get("port"))
});