const { Schema, model } = require("mongoose");

const Pet = new Schema(
    {
        name: { type: String },
        type:{
            type: String,
            enum: ["Dog","Cat","Fish"]
        },
        breed: { type: String }
    },
    {
        strict: true
    }
)

module.exports = model("Pet", Pet);