const { Schema, model } = require("mongoose");

const Customer = new Schema(
    {
       name:        { type: String, required: true, trim: true},
       lastname:    { type: String, required: true, trim: true},
       phone:       { type: Number },
       email:       { type: String },
       address:     { type: String },
       pet_id:      {type: String, requiered: true}
    },
    {
        strict: true
    }
)



module.exports = model("Customer", Customer);