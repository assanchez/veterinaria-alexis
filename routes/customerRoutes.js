const {Router}  = require("express");
const router = Router()

const customerController = require("../controllers/CustomerController.js");

router.get("/", customerController.getCustomers);
//router.get("/:id", customerController.getCustomerById);
router.get("/:id", customerController.getCustomerByIdWithPets);
router.post("/", customerController.post);
router.delete("/:id", customerController.delete);
router.patch("/:id", customerController.patch);

module.exports = router;