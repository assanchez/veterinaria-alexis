const {Router}  = require("express");
const router = Router()

const petController = require("../controllers/PetController.js");

router.get("/", petController.getPets);
router.get("/:id", petController.getPetById);
router.post("/", petController.post);
router.delete("/:id", petController.delete);
router.patch("/:id", petController.patch);

module.exports = router;