const express = require("express")
const app = express()

const { json, urlencoded } = require("body-parser")
const cors = require("cors")
const morgan = require("morgan")
const mongoose = require("mongoose")

const customerRoute = require("./routes/customerRoutes.js");
const petsRoute = require("./routes/petRoutes");

app.set("port", 3000);
app.use(cors())
app.use(json())
app.use(urlencoded({extended: false}))
app.use(morgan("dev"))

mongoose.connect("mongodb://localhost:27017/Veterinaria").then((db)=>{
    console.log("Conectado a Mongo")
})
.catch((err) => {
    console.error("Error al conectarse a mongodb: ", err)
});

app.use("/Customer",customerRoute);
app.use("/Pet",petsRoute);


module.exports = app;