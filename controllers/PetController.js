const Pet = require("../models/PetModel.js")
const petController = {}

petController.getPets = async (req,res) =>{
    try{
        res.json(await Pet.find({}));
    }catch(err){
        res.status(500).json(err);
    }
}

petController.post = async (req,res) =>{
    try{
        var petExpect = {} 
        var expectedParams= [
            "name",
            "type",
            "breed"
        ]
        Object.keys(req.body).forEach((key)=>{
            if (expectedParams.includes(key)) 
                petExpect[key] = req.body[key]; 
        });

        let pet = new Pet(petExpect)
        await pet.save()

        res.json(pet);
    }catch(err){
        res.status(500).json(err);
    }
};

petController.getPetById = async (req,res) => {
    const { id } = req.params;
    try{
        let pet = await Pet.findById(id);
        if (pet)
            res.json(customer);
        else
            res.json({status:400, message: "Pet Not Found"});
    }catch(err){
        res.status(500).json({err});  
    }
}

petController.delete = async(req,res) => {
    const { id } = req.params;
    try{
        await Pet.deleteOne({_id : id});
        res.json({status:200, message: "Pet Removed"});
    }catch(err){
        res.status(404).json({err}); 
    }
}

petController.patch = async (req,res) =>{
    try{
        const { id } = req.params;
        var petExpect = {} 
        var expectedParams= [
            "name",
            "type",
            "breed"
        ]
        Object.keys(req.body).forEach((key)=>{
            if (expectedParams.includes(key)) 
                petExpect[key] = req.body[key]; 
        });

        await Pet.updateOne({_id: id},petExpect)
        res.json({message: "Pet Updated"});
    }catch(err){
        res.status(500).json(err);
    }
};

module.exports = petController;