const Customer = require("../models/CustomerModel")
const Pet = require("../models/PetModel")
const customerController = {}

customerController.getCustomers = async (req,res) =>{
    try{
        let customer = await Customer.find({});
        res.json(customer)
    }catch(err){
        res.status(500).json(err);
    }
};

customerController.post = async (req,res) =>{
    try{
        var customerExpect = {} 
        var expectedParams= [
            "name",
            "lastname",
            "phone",
            "email",
            "address",
            "pet_id"
        ]
        Object.keys(req.body).forEach((key)=>{
            if (expectedParams.includes(key))
                customerExpect[key] = req.body[key]; 
        });

        let customer = new Customer(customerExpect)
        await customer.save()

        res.json(customer);
    }catch(err){
        res.status(500).json(err);
        console.log(err);
    }
};

customerController.getCustomerById = async (req, res) =>{
    const { id } = req.params;
    try{
        let customer = await Customer.findById(id);
        if (customer)
            res.json(customer);
        else
            res.json({status:400, message: "No se encontró el customer"});
    }catch(err){
        res.status(500).json({err}); 
    }
}

customerController.getCustomerByIdWithPets = async (req, res) =>{
    const { id } = req.params;
    try{
        let customer = await Customer.findById(id);
        if (customer){
            let pet = await Pet.findOne({_id: customer.pet_id})
            res.json({
                "customer" : customer,
                "pet"      : pet
            });
        }
            
        else
            res.json({status:400, message: "No se encontró el customer"});
    }catch(err){
        res.status(500).json({err}); 
    }
}

customerController.delete = async(req,res) => {
    const { id } = req.params;
    try{
        let remove = await Customer.deleteOne({_id : id});
        res.json({status:200, message: "Se eliminó al customer correctamente"});
    }catch(err){
        res.status(404).json({err}); 
    }
}

customerController.patch = async(req,res) => {
    const { id } = req.params;
    try{
        var expectedParams= [
            "name",
            "lastname",
            "phone",
            "email",
            "address",
            "pet_id",
        ]
        var customerExpect = {}
        Object.keys(req.body).forEach((key)=>{
            if (expectedParams.includes(key))
                customerExpect[key] = req.body[key]; 
        });

        await Customer.updateOne({_id: id},customerExpect);
        res.json({message: "Actualizado correctamente"});
    }catch(err){
        res.status(500).json({err});
    }
}

module.exports = customerController;